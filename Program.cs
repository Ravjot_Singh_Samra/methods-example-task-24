﻿using System;

namespace methods_example_task_24
{
    class Program
    {
        public static void fMethod()
        {
            Console.WriteLine("My first method");
            Console.WriteLine("My first method");
            Console.WriteLine("My first method");
            Console.WriteLine("My first method");
            Console.WriteLine("My first method");
        }

        public static void nameGreet(string name)
        {
            Console.WriteLine($"Hello {name}");
        }
        
        public static string nameGreet2(string lastName, int age)
        {
            return $"Hello {lastName} and you are {age} years old. nameGreet2 method.";
        }

        public static string multiNum(int A, int B, int C)
        {
            return $"{A * 3} {B * 3} {C * 3}";
        }

        public static void countUntilNum(int X)
        {
            for (var Y = 0; Y < X; Y++)
            {
                var Z = Y + 1;
                Console.WriteLine($"Counting to {X}, current number: {Z}");
            }
        }
        public static void Main(string[] args)
        {
            fMethod();
            //fMethod(); // to see if it is actually working by calling it twice, remove // to activate

            Console.WriteLine("\n");
            
            nameGreet("Ravjot");
            nameGreet("John");
            nameGreet("Joe");
            nameGreet("Bob");
            nameGreet("Jane");
            Console.WriteLine(nameGreet2("Smith", 23));
            Console.WriteLine(multiNum(3, 4, 5));
            Console.WriteLine(multiNum(6, 3, 9));
            Console.WriteLine(multiNum(11, 14, 15));
            Console.WriteLine(multiNum(31, 42, 53));
            Console.WriteLine(multiNum(33, 12, 5));
            countUntilNum(10);
        }
    }
}

